import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CatsService } from './cats.service';
import { CatsController } from './cats.controller';
import { UploadController } from './upload.controller';
import { UploadModule } from './upload/upload.module';

@Module({
  imports: [UploadModule],
  controllers: [AppController,CatsController,UploadController],
  providers: [AppService,CatsService],
})
export class AppModule {}
