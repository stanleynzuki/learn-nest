import {Controller,Get,Post,Body,Put, Delete} from '@nestjs/common';
import { CatsService } from './cats.service';


@Controller()
export class CatsController{
    constructor (private readonly catsService: CatsService){}
    @Get("/all_cats")
    getCats(){
        var cats = this.catsService.getCats()
        return cats;
    }

    @Post('/add_cat')
    addCat(@Body() catData: any){
        try {
            // Assuming you have a service method for adding a cat
            const newCat = this.catsService.addCat(catData);
            return newCat;
          } catch (e) {
            return e.toString();
          }
    }

    @Put('/edit_cat')
    editCat(@Body() catData: any){
        try {
            // Assuming you have a service method for adding a cat
            const newCat = this.catsService.editCat(catData);
            return newCat;
          } catch (e) {
            return e.toString();
          }
    }

    @Delete('/delete_cat')
    deleteCat(@Body() catData: any){
        try{
            const newCat = this.catsService.deleteCat(catData);
            return newCat;
        }catch(e){
            return e.toString();
        }
    }

    
}
