import {Injectable} from '@nestjs/common';
import { diskStorage } from 'multer';

var cats = [];
@Injectable()
export class CatsService{
    
    getCats(){
        return cats
    }

    addCat(catData : any){
        var id = 1
        if(cats.length>0){
            id = cats.length +1
        }
        catData['id'] = id
        cats.push(catData);
        return cats
    }

    editCat(catData: any){
       
        var id = catData['id']
        var name = catData['name']
        const index = this.getCats().findIndex((cat)=>cat.id === parseInt(id));
        this.getCats()[index]['name']= name;
        return this.getCats()
    }

    deleteCat(catData: any){
        var id = parseInt(catData['id']);
        const index = this.getCats().findIndex((cat)=>cat.id === id);
        this.getCats().splice(index,1);
        return this.getCats()
    }

    uploadImage(catData: any){
        
    }

    

}
