import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as multer from 'multer';


async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const storage = multer.memoryStorage(); // You can configure storage options as needed
  const multerMiddleware = multer({ storage });

  app.use(multerMiddleware.single('file'));
  await app.listen(3000);
}
bootstrap();
