import { Controller, Post, UploadedFile, UseInterceptors,Res,Get } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { Response } from 'express';
import * as fs from 'fs';

@Controller('upload')
export class UploadController {
  @Post()
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './uploads',
        filename: (req, file, cb) => {
          const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9);
          cb(null, `${uniqueSuffix}-${file.originalname}`);
        },
      }),
    }),
  )
  uploadFile(@UploadedFile() file) {
    // Handle the uploaded file here
    // You can access the file using 'file' variable
    // Perform any necessary processing or saving of the file

    return { message: 'File uploaded successfully', filename: file.filename };
  }

  @Get('/upload')
  async serveHtml(@Res() res: Response): Promise<void> {
    // You can read and serve an HTML file
    const htmlContent = fs.readFileSync('./public/index.html', 'utf8');

    // Or generate HTML content dynamically
    // const htmlContent = '<html><body><h1>Hello, NestJS!</h1></body></html>';

    res.send(htmlContent);
  }
}
